/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout;
    QVBoxLayout *right;
    QFrame *info;
    QLabel *who;
    QPushButton *button0;
    QPushButton *treyb;
    QPushButton *sizeb;
    QPushButton *closeb;
    QFrame *line;
    QLabel *label;
    QFrame *write;
    QTextEdit *textW;
    QPushButton *pushButton_2;
    QPushButton *send_b;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QFrame *line_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QTextEdit *textEdit_2;
    QPushButton *pushButton;
    QFrame *line_3;
    QListView *listmessege;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(750, 450);
        MainWindow->setMinimumSize(QSize(700, 450));
        MainWindow->setMaximumSize(QSize(750, 554645));
        MainWindow->setAcceptDrops(false);
        MainWindow->setInputMethodHints(Qt::ImhNone);
        MainWindow->setTabShape(QTabWidget::Rounded);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        right = new QVBoxLayout();
        right->setSpacing(6);
        right->setObjectName(QStringLiteral("right"));
        info = new QFrame(centralWidget);
        info->setObjectName(QStringLiteral("info"));
        info->setMinimumSize(QSize(350, 40));
        info->setMaximumSize(QSize(16777215, 40));
        info->setBaseSize(QSize(350, 75));
        info->setFrameShape(QFrame::NoFrame);
        info->setFrameShadow(QFrame::Raised);
        who = new QLabel(info);
        who->setObjectName(QStringLiteral("who"));
        who->setGeometry(QRect(10, 0, 121, 41));
        who->setMinimumSize(QSize(50, 25));
        who->setMaximumSize(QSize(16777215, 50));
        QFont font;
        font.setPointSize(14);
        who->setFont(font);
        button0 = new QPushButton(info);
        button0->setObjectName(QStringLiteral("button0"));
        button0->setGeometry(QRect(330, 0, 25, 25));
        button0->setMinimumSize(QSize(25, 25));
        button0->setMaximumSize(QSize(25, 20));
        QFont font1;
        font1.setPointSize(15);
        button0->setFont(font1);
        treyb = new QPushButton(info);
        treyb->setObjectName(QStringLiteral("treyb"));
        treyb->setGeometry(QRect(360, 0, 25, 25));
        treyb->setMinimumSize(QSize(25, 25));
        treyb->setMaximumSize(QSize(25, 25));
        treyb->setFont(font);
        sizeb = new QPushButton(info);
        sizeb->setObjectName(QStringLiteral("sizeb"));
        sizeb->setGeometry(QRect(390, 0, 25, 25));
        sizeb->setMinimumSize(QSize(25, 25));
        sizeb->setMaximumSize(QSize(25, 25));
        sizeb->setFont(font);
        closeb = new QPushButton(info);
        closeb->setObjectName(QStringLiteral("closeb"));
        closeb->setGeometry(QRect(420, 0, 25, 25));
        closeb->setMinimumSize(QSize(25, 25));
        closeb->setMaximumSize(QSize(25, 25));
        closeb->setFont(font);

        right->addWidget(info);

        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setMinimumSize(QSize(450, 0));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        right->addWidget(line);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(350, 0));

        right->addWidget(label);

        write = new QFrame(centralWidget);
        write->setObjectName(QStringLiteral("write"));
        write->setMinimumSize(QSize(420, 160));
        write->setMaximumSize(QSize(16777215, 190));
        write->setFrameShape(QFrame::NoFrame);
        write->setFrameShadow(QFrame::Raised);
        textW = new QTextEdit(write);
        textW->setObjectName(QStringLiteral("textW"));
        textW->setGeometry(QRect(0, 40, 450, 111));
        textW->setMinimumSize(QSize(350, 0));
        textW->setMaximumSize(QSize(450, 150));
        textW->setBaseSize(QSize(350, 0));
        pushButton_2 = new QPushButton(write);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(10, 10, 31, 27));
        pushButton_2->setFont(font1);
        pushButton_2->setStyleSheet(QStringLiteral("border-image: url(:/icons/smile);"));
        send_b = new QPushButton(write);
        send_b->setObjectName(QStringLiteral("send_b"));
        send_b->setGeometry(QRect(360, 160, 81, 27));
        pushButton_4 = new QPushButton(write);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(50, 10, 31, 27));
        pushButton_4->setStyleSheet(QStringLiteral("border-image: url(:/icons/folder);"));
        pushButton_5 = new QPushButton(write);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(90, 10, 31, 27));
        pushButton_5->setStyleSheet(QStringLiteral("border-image: url(:/icons/haird);"));
        pushButton_6 = new QPushButton(write);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setGeometry(QRect(130, 10, 31, 27));
        pushButton_6->setStyleSheet(QLatin1String("\n"
"border-image:url(:/icons/bubble.png);"));
        line_2 = new QFrame(write);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(0, -10, 450, 20));
        line_2->setMinimumSize(QSize(450, 0));
        line_2->setAutoFillBackground(false);
        line_2->setStyleSheet(QStringLiteral(""));
        line_2->setLineWidth(0);
        line_2->setMidLineWidth(1);
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        right->addWidget(write);


        gridLayout->addLayout(right, 1, 1, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        textEdit_2 = new QTextEdit(centralWidget);
        textEdit_2->setObjectName(QStringLiteral("textEdit_2"));
        textEdit_2->setMaximumSize(QSize(300, 30));

        horizontalLayout->addWidget(textEdit_2);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setMaximumSize(QSize(25, 25));
        pushButton->setFont(font1);

        horizontalLayout->addWidget(pushButton);


        verticalLayout->addLayout(horizontalLayout);

        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setStyleSheet(QStringLiteral("background-color: transparent;"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_3);

        listmessege = new QListView(centralWidget);
        listmessege->setObjectName(QStringLiteral("listmessege"));

        verticalLayout->addWidget(listmessege);


        gridLayout->addLayout(verticalLayout, 1, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout);

        MainWindow->setCentralWidget(centralWidget);
        QWidget::setTabOrder(button0, textEdit_2);
        QWidget::setTabOrder(textEdit_2, pushButton);
        QWidget::setTabOrder(pushButton, textW);
        QWidget::setTabOrder(textW, pushButton_2);
        QWidget::setTabOrder(pushButton_2, send_b);
        QWidget::setTabOrder(send_b, pushButton_4);
        QWidget::setTabOrder(pushButton_4, pushButton_5);
        QWidget::setTabOrder(pushButton_5, pushButton_6);
        QWidget::setTabOrder(pushButton_6, treyb);
        QWidget::setTabOrder(treyb, sizeb);
        QWidget::setTabOrder(sizeb, closeb);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "EChat", 0));
        who->setText(QApplication::translate("MainWindow", "File Transfer", 0));
        button0->setText(QApplication::translate("MainWindow", "\342\206\223", 0));
        treyb->setText(QApplication::translate("MainWindow", "-", 0));
        sizeb->setText(QApplication::translate("MainWindow", "\342\226\240", 0));
        closeb->setText(QApplication::translate("MainWindow", "\321\205", 0));
        label->setText(QString());
        pushButton_2->setText(QString());
        send_b->setText(QApplication::translate("MainWindow", "send", 0));
        pushButton_4->setText(QString());
        pushButton_5->setText(QString());
        pushButton_6->setText(QString());
        pushButton->setText(QApplication::translate("MainWindow", "+", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
