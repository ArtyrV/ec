#include "autoriz.h"
#include "ui_Autorization.h"
#include <QPushButton>
#include <QComboBox>
#include <QDialog>
#include <QDesktopWidget>
#include <QStyle>
#include <QLineEdit>

autoriz::autoriz(QWidget *parent) :QDialog(parent), gui(new Ui::Form)
{
	
	
	this->setGeometry(
		QStyle::alignedRect(
		Qt::LeftToRight,
		Qt::AlignCenter,
		this->size(),
		qApp->desktop()->availableGeometry()
		)
	);

	gui->setupUi(this);
	connect(gui->pb, SIGNAL(clicked()), this, SLOT(close()));


	cb = new QComboBox(this);
	lephon = new QLineEdit(this);


	this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

	sc=QStringList() << tr("UA +380") << tr("RU +") << tr("UK +");
	
	cb->addItems(sc);
	gui->ugl_in_m->addWidget(cb,0,Qt::AlignLeft);
	gui->ugl_in_m->addWidget(lephon);
	
}


autoriz::~autoriz()
{
	delete gui;
}
