#include "mainwindow.h"
#include "ui_mainwindow.h"
//Tray
void MainWindow::showTrayIcon()
{
	// ������ ��������� ������ � ����� ��� ��������...
	trayIcon = new QSystemTrayIcon(this);
	QIcon trayImage(":/icons/ico");
	trayIcon->setIcon(trayImage);
	trayIcon->setContextMenu(trayIconMenu);

	// ���������� ���������� ����� �� ������...
	connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayIconActivated(QSystemTrayIcon::ActivationReason)));

	// ������� ������...
	trayIcon->show();
}

void MainWindow::trayActionExecute()
{

	Q_EMIT this->showNormal();
}

void MainWindow::trayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
	switch (reason)
	{
	case QSystemTrayIcon::Trigger:
	case QSystemTrayIcon::DoubleClick:
		this->trayActionExecute();
		break;
	default:
		break;
	}
}

void MainWindow::setTrayIconActions()
{
	//QTextCodec::setCodecForLocale(QTextCodec::codecForName("Windows-1251"));
	// Setting actions...
	
    minimizeAction = new QAction(tr("Roll up"), this);
    restoreAction = new QAction(tr("Restore"), this);
    quitAction = new QAction(tr("Close"), this);

	// Connecting actions to slots...
	connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));
	connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));
	connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));

	// Setting system tray's icon menu...
	trayIconMenu = new QMenu(this);
	trayIconMenu->addAction(minimizeAction);
	trayIconMenu->addAction(restoreAction);
	trayIconMenu->addAction(quitAction);
}

void MainWindow::changeEvent(QEvent *event)
{
	QMainWindow::changeEvent(event);
	if (event->type() == QEvent::WindowStateChange)
	{
		if (isMinimized())
		{
			this->hide();
		}
	}
}
